# 1.0.0 (2020-04-29)


### Bug Fixes

* Support links to Markdown pages ([8a8e87b](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/8a8e87bf303b481a2dc4eb25b79a3a258868a25f))
