<!-- Start by making a copy of the component template to your own drafts.

In Figma:
1. Duplicate the component template https://www.figma.com/file/OmvFfWkqEsdGhXAND133ou/Component?node-id=0%3A1
2. Open the duplicate, then use the dropdown next to the file name to select “Move to Project…”
   and select your Drafts as the new location.
3. Update the template with your component name and start designing ;)
-->

<!--Add a short description of the component. If it’s helpful, add a checklist of variations
and states to the description so that a reviewer can be sure to cross reference everything
that has been completed.-->

<!--Use the Figma share feature and make sure that “anyone with the link” can view. Then,
specifically invite the reviewer with “edit” permissions selected. Anyone can duplicate the
file to their own drafts and edit from there, but the reviewer can directly edit and
collaborate on the file. This will help maintain the integrity of the initial draft.-->

[View component in Figma →](ADD LINK TO FIGMA FRAME)

### Checklist

Make sure the following are completed before closing the issue:

1. [ ] **Assignee**: Create component in your own draft file in Figma using the
[component template](https://www.figma.com/file/OmvFfWkqEsdGhXAND133ou/%5BComponent%5D),
including all variations and states.
1. [ ] **Assignee**: Update the link to the Figma file in the issue description.
1. [ ] **Assignee**: Ask a [FE/UX Foundations designer](https://about.gitlab.com/company/team/?department=fe-ux-foundations-team)
to review your component (ensure they have edit permissions in Figma).
1. [ ] **Reviewer**: Review and approve assignee’s addition. Ensure that component
includes all variations/states and, if applicable, matches existing Sketch specs and
is responsive.
1. [ ] **Reviewer**: Assign to a [Figma maintainer](https://about.gitlab.com/handbook/engineering/projects/#design.gitlab.com) for final review (make sure they have edit permissions in Figma).
1. [ ] **Maintainer**: Review and approve assignee’s addition.
1. [ ] **Maintainer**: Add the component to the **Pajamas UI Kit** file, and view
the component in the Assets panel to ensure it aligns with what’s outlined in the
[document and asset library structure](https://gitlab.com/gitlab-org/gitlab-design/-/blob/master/CONTRIBUTING-Figma.md#document-and-asset-library-structure) documentation.
1. [ ] **Maintainer**: Publish the library changes along with a clear commit message.
1. [ ] **Assignee**: Move the draft file to the **Component archive** Figma project. If you're a community contributor, we ask that you [transfer ownership of your draft file](https://help.figma.com/hc/en-us/articles/360040530853) to the maintainer so they can move it to our archive, along with its version history and comments.
1. [ ] **Assignee** (or Maintainer, for community contributions): If it's a new
pattern or a significant change, add an agenda item to the next UX weekly call
to inform everyone.
1. [ ] **Assignee**: Create a merge request in this repository with the [component-guideline template](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/blob/master/.gitlab/merge_request_templates/component-guideline.md)
to create or update the component's documentation page. Link it here as a related
merge request. Use `View design in Pajamas UI Kit →` for the link text. This replaces
any link to Sketch Measure specs. Anyone with the link should be able to view the file. 
1. [ ] **Assignee**: Once the merge request is created, close this issue. 

/label ~"UX" ~"Figma"
