# Pajamas Design System

Resources, components, and design guidelines behind GitLab. See [design.GitLab.com](https://design.gitlab.com).
Read about the [Component Lifecycle](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/blob/master/doc/component-lifecycle.md).

## Prerequisites

Before you move on, make sure you have [Node](https://nodejs.org/en/) 12.x and [Yarn](https://yarnpkg.com/) 1.10 or newer installed.

### Start using Git on the command line

- [Start using Git](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)
- [Command Line basic commands](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html)
- [Technical Tutorials for Product Designers](https://www.youtube.com/playlist?list=PL05JrBw4t0Kra6RseLWXFIXtu9UPzjzbT) on Unfiltered

## Quick start

1. Clone the project with `git clone git@gitlab.com:gitlab-org/gitlab-services/design.gitlab.com.git`
1. Install [yarn](https://yarnpkg.com/en/docs/install)
1. Run `yarn install`
1. Run `yarn start` to start build & watch changes
1. Open http://localhost:2018

## Linting

We control this repositories code quality with automatic linting tools like `eslint` and `prettier`.

If you change something in the code, you can run `yarn test` to see if your changes are up to code.

A lot of failures are auto-fixable, so simply run `yarn run lint:fix` to fix errors automatically.

## Creating a Figma component

To create a new Figma component, [create an issue with the Figma component template](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/-/issues/new?issuable_template=Figma%20component) and follow the steps in the template. 

## Writing component documentation

When starting from scratch:
1. Copy the contents from `pages/components/template.md`
1. Paste the contents in an existing placeholder file in `contents/components/` or create a new file and call it `componentname.md`
1. Edit and write your documentation

## Reviewing

Each merge request for this repository will generate a [review app](https://docs.gitlab.com/ee/ci/review_apps/). Review apps create a new environment (dynamically) for each one of your branches. A Review App is then visible as a link when you visit the merge request relevant to the branch. That way, you are able to see live all changes introduced by the merge request changes.

## Contributing

GitLab is an open source project and we are very happy to accept community
contributions. Please refer to [CONTRIBUTING.md](/CONTRIBUTING.md) for details.

## Why is the design system called Pajamas?

Pajamas is a nod to our fully remote culture. In addition, they are comfortable, reliable, and consistent! Read more about how we landed on the name Pajamas in the [original issue](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/issues/138).
